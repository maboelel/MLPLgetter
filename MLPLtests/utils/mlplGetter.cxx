/// @file mlplGetter.cxx
/// @brief Reading the DIPZ labels of an event and output the calculate its MLPL discriminant variable
/// The DIPZ labels are: HLT_AntiKt4EMTopoJets_subjesIS_fastftagAuxDyn.dipz20230223_z & HLT_AntiKt4EMTopoJets_subjesIS_fastftagAuxDyn.dipz20230223_negLogSigma2

// System include(s):
#include <memory>
#include <vector>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <stdlib.h>


// ROOT include(s):
#include <TFile.h>
#include <TError.h>

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// EDM include(s):
#include "AthContainersInterfaces/AuxTypes.h"
#include "xAODCore/tools/PerfStats.h"
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/Jet.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"

int main( int argc, char* argv[] ) {

   // The name of the application:
   static const char* APP_NAME = "MlplGetter";

   // Check that at least one input file was provided:
   if( argc < 2 ) {
      Error( APP_NAME, "Usage: %s <file1> [file2] ...", APP_NAME );
      return 1;
   }

   // Set up the environment:
    xAOD::Init().ignore();

   // Set up the event object:
   xAOD::TEvent event( xAOD::TEvent::kClassAccess );

   // Start the measurement:
   auto& ps = xAOD::PerfStats::instance();
   ps.start();

   // Loop over the specified files:
   //for( int i = 1; i < argc; ++i ) {

      // Open the file:
      std::unique_ptr< TFile > ifile( TFile::Open( argv[ 1 ], "READ" ) );
      if( ( ! ifile.get() ) || ifile->IsZombie() ) {
         Error( APP_NAME, "Couldn't open file: %s", argv[ 1 ] );
         return 1;
      }
      Info( APP_NAME, "Opened file: %s", argv[ 1 ] );

      // Connect the event object to it:
      event.readFrom( ifile.get() ).ignore();

      // Loop over its events:
      //const Long64_t entries = event.getEntries();
      //for( Long64_t entry = 0; entry < entries; ++entry ) {

         Long64_t entry = strtol(argv[2], NULL, 10);

         
         // Load the event:
         if( event.getEntry( entry ) < 0 ) {
            Error( APP_NAME, "Couldn't load entry %lld from file: %s",
                   entry, argv[ 1 ] );
            return 1;
         }

         // Print some status:
         /*if( ! ( entry % 500 ) ) {
            Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
         }
         */

         // Read the Jet Container
         const xAOD::JetContainer* jets = 0;
         event.retrieve( jets, "HLT_AntiKt4EMTopoJets_subjesIS_fastftag" ).ignore();

         std::cout << "The number of the jets in this event is: " << jets->size() << " jets" << std::endl;
         
         std::vector<float> mlpl_array;

         // MLPL (4,ALL) Calculation
         for (long unsigned int i = 0; i < jets->size(); ++i) {
            for (long unsigned int j = i + 1; j < jets->size(); ++j) {
               for (long unsigned int k = j + 1; k < jets->size(); ++k) {
                  for(long unsigned int l = k + 1; l < jets->size(); ++l) {
                        float num = 0.0;
                        float denom = 0.0;
                        float second_term = 0.0;
                        float third_term = 0.0;
                        std::vector<long unsigned int> indices = {i,j,k,l};


                        for (long unsigned int m : indices) {
                           float mu = (*jets)[m]->getAttribute<float>("dipz20230223_z") * 50.0;
                           float sigma = std::exp(-0.5 *(*jets)[m]->getAttribute<float>("dipz20230223_negLogSigma2")) * 50.0;

                           num += mu / (sigma * sigma);
                           denom += 1.0 / (sigma * sigma);
                           second_term -= std::log(sigma);
                        }

                        for (long unsigned int m : indices) {  
                           float mu = (*jets)[m]->getAttribute<float>("dipz20230223_z") * 50.0;
                           float sigma = std::exp(-0.5 *(*jets)[m]->getAttribute<float>("dipz20230223_negLogSigma2")) * 50.0;

                           float mean = num / denom;
                           third_term -= (mean - mu) * (mean - mu) / (2.0 * sigma * sigma);
                        }

                        mlpl_array.push_back(-4.0 * std::log(std::sqrt(2.0 * M_PI)) + second_term + third_term);
                  }
               }
            }
         }

         double max_log_likelihood = *std::max_element(mlpl_array.begin(), mlpl_array.end());

         std::cout << "MLPL(4,All) score of the event number (" << entry << ") is: " << max_log_likelihood << std::endl;

         // Loop over the jets in the event
         /*for(const xAOD::Jet* jet : * jets ){
         std::cout << "Let's go!" << std::endl;
         std::cout << jet->getAttribute<float>("dipz20230223_z") << std::endl;
         std::cout << jet->getAttribute<float>("dipz20230223_negLogSigma2") << std::endl;
         }
         */
      //}
   //}

   // Stop the measurement:
   ps.stop();
   xAOD::IOStats::instance().stats().Print( "Summary" );

   // Return gracefully:
   return 0;
}
